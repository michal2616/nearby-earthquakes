# Readme!

# How to build and run application!
  - Application to work requires Java 11 and Maven installed
  - To be able to build it go to the folder with pom.xml and run command:
```sh
$ mvn clean install
```
  - To run app execute command:
```sh
$ java -jar target/nearby-earthquakes-1.0-jar-with-dependencies.jar
```
# How to use app
After running app provide coordinates latitude and longitude separated by space character e.g:
```sh
50.049683 19.944544
```